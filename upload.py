import os
import time
from flask import Flask, request, flash, redirect, url_for
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = 'uploads'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'dmg'])
CHUNK_SIZE = 5000

app = Flask(__name__, static_url_path='/upload')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def read_in_chunks(file_object):
    """Lazy function (generator) to read a file piece by piece.
    Default chunk size: 5k."""
    while True:
        data = file_object.read(CHUNK_SIZE)
        if not data:
            break
        yield data


@app.route('/upload', methods=['POST'])
def upload_file():
    # checking if the file is present or not.
    if 'file' not in request.files:
        return "No file found"
    file = request.files['file']
    filename = secure_filename(file.filename)
    if filename == '':
        flash('No selected file')
        return redirect(request.url)
    if file:
        if not allowed_file(filename):
            return "Invalid file format"
        # t1 = time.time()
        with open(os.path.join(app.config['UPLOAD_FOLDER'], filename), 'wb') as fo:
            for piece in read_in_chunks(file):
                fo.write(piece)
        # print(time.time() - t1)
        return "file successfully saved"


if __name__ == '__main__':
    app.run(debug=True)
